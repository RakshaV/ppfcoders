package com.example.demo.entities;

import java.sql.Timestamp;

public class Stocks {

	private int id;
	private String company;
	private String buySell;
	private String stockTicker;
	private double price;
	private int volume;
	private int statusCode;
	private String category;
	private Timestamp dtime;
	
	public int getId() {
		return id;
	}
	
	public Stocks() {
		
	}
	
	public Stocks(int id,String company,String buySell,String stockTicker,double price,int volume,int statusCode,String category,Timestamp dtime) {
		super();
		this.id = id;
		this.company=company;
		this.stockTicker = stockTicker;
		this.buySell = buySell;
		this.price=price;
		this.statusCode=statusCode;
		this.volume=volume;
		this.category=category;
		this.dtime=dtime;
		
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getstockTicker() {
		return stockTicker;
	}
	public void setstockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}
	public String getbuySell() {
		return buySell;
	}
	public void setbuySell(String buySell) {
		this.buySell = buySell;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Timestamp getDtime() {
		
		return dtime;
	}

	public void setDtime(Timestamp dtime) {
		this.dtime = dtime;
	}

	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}

}
