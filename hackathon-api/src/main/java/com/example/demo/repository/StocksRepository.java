package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.*;


@Component
public interface StocksRepository {
	public List<Stocks> getAllStocks();
	public Stocks getStocksById(int id);
	public Stocks editStocks(Stocks stock);
	public int deleteStocks(int id);
	public Stocks addStocks(Stocks stock);

}
